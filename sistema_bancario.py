class Sistema: # Nivel 0 > NAO LOGADO | Nivel 1 > LOGADO
    def __init__(self):
        # Dica > Carregar o banco de dados com as contas.
        pass

    def menu_principal(self): # Lembrem que o usuario é burro, tratem os dados bem.
        """O menu principal deve listar primeiramente duas opçoes, Criar conta e Logar na conta.
        Depois que o login é efetuado as outras opçoes sao liberadas.
        """
        pass
    
    def acessar_conta(self): # Nivel 0
        # Dica > Bons filtros de input, evitam problemas.    
        pass
    
    def criar_conta(self): # Nivel 0
        # Dica > A funçao criar_conta deve acessar a classe Conta.
        pass

    def cambio_de_dinheiro(self): # Nivel 1
        """Tipos de Moedas >> Principal > Real, Secundarias > Libra, Euro, Dolar"""
        # Dica > O cambio acessa o saldo da conta, e converte para uma outra moeda.
        pass

    def alterar_dados(self): # Nivel 1
        """A memoria tem que dar a possibilidade ao usuario de alterar dados da conta.
        Nome ou senha. Para alterar o nome custa 5 dolares, e a senha custa 4 euros.
        """
        pass

class Conta: # A conta tem 4 tipos de carteira. Real, Dolar, Euro e Libra.
    def __init__(self, id, nome, senha, cpf, saldo):

        pass
    def extrato(self):
        pass

    def sacar(self):
        pass

    def depositar(self):
        pass

    def transferir(self):
        pass

class Loja: # Nivel 2
    """A loja é uma opçao do menu, o usuario precisa logar em sua conta para visualizar ou comprar um produto."""
    def __init__(self):
        # Dica > Loja Carrega uma lista de produtos
        pass

    def menu_principal(self):
        pass    
    
    def carrinho(self):
        # O usuario pode escolher mais de um produto para comprar.
        pass
    
    def comprar(self):
        pass
